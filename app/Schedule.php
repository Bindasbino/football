<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    public function teams(){
        return $this->belongsToMany('App\Team');
    }

    public function stages(){
        return $this->belongsToOne('App\Stage');
    }
}
