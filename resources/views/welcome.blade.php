<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset(STATIC_DIR.'css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>Mayor Cup</title>
    <link rel="icon" href="" type="image/gif" sizes="16x16">
    <style type="text/css">
      .nav_modify{
            background-color: rgb(0,0,0,1) !important;
      }
     
      .nav-link:hover{

      color: yellow !important;

      }


    /* .arrow{
    position: absolute;
    margin:-50px 0px 0px 100px;
    
      } 
    
       .arrow:hover{
    color: red;
       } */

     
    
    </style>

   
  </head>


  <body >
    <nav class="navbar navbar-expand-lg navbar-dark  nav_modify">
  <a class="navbar-brand" href="#">MayorCup2075</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Team<span class="sr-only">(current)</span></a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="#">Points<span class="sr-only">(current)</span></a>
      </li>
      
     
    </ul>
    <ul class="navbar-nav ml-auto" >
      <li class="nav-item active" style="color:#fff;margin-right:5px;">Follow us on</li>
      <li class="nav-item active">
       <a class="" href="#" style="margin:0px 1px 0px 0px;color:#fff;background:blue;padding:5px 11px;border-radius:50%;"> <i class="fab fa-facebook-f"></i></a>
        <a class="" href="#" style="margin:0px 10px 0px 1px;color:red;background:#fff;padding:6px 8px;border-radius:50%;" ><i class="fab fa-youtube"></i></a>
      </li>
      
      
    </ul>
    
  </div>
</nav>
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{asset(STATIC_DIR.'backend/plugins/images/banner.jpg')}}" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset(STATIC_DIR.'backend/plugins/images/banner.jpg')}}" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset(STATIC_DIR.'backend/plugins/images/banner.jpg')}}" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    <div class="container-fluid" style="background:#000;" >
      <div class="container" >
    <div class="col-md-12 col-sm-12 col-xl-12 ">
      

      <div class="row pt-2 text-center" >
        <table class="table">
         
            <th style="border:none;"><a href="" class="btn btn-sm btn-outline-primary active ">Match Result</a></th>
          <th style="border:none;"><a href="" class="btn btn-sm btn-outline-primary  ">Next Match</a></th>
          <th style="border:none;"><a href="" class="btn btn-sm btn-outline-primary ">Last Match</a></th>
          
          
        </table>
        
      </div>
       
       
        <table class="table table-dark text-center" >
            
               <div class="pt-0">
                     <thead>
                       <tr  style="background:yellow;color:black;">
                           
                           <th colspan="3">Match 1 - माघ १०,२०७५</th>
                           
                       </tr>
                   </thead>
                   
                   <tbody>
                       <tr class="sticky">
                           <td><img src="{{asset(STATIC_DIR.'backend/plugins/images/users/varun.jpg')}}" class="img img-responsive" alt="" style="width:50px;border-radius:50%;"><br> Ward No. 1</td>
                           <td style="padding-top:20px;"><strong style="background:#000;padding:5px 10px;"> 2 - 3</strong><h2 style="margin-top:5px;" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-chevron-circle-down arrow" id="model"></i></h2></td>
                           <td> <img src="{{asset(STATIC_DIR.'backend/plugins/images/users/varun.jpg')}}" class="img img-responsive" alt="" style="width:50px;border-radius:50%;"><br> Ward No. 2</td>

                       </tr>
                   </tbody>
                   
                   <thead>
                       <tr  style="background:yellow;color:black;">
                           
                           <th colspan="4">Match 1</th>
                           
                       </tr>
                   </thead>
                   
                   <tbody>
                       <tr>
                           <td><img src="{{asset(STATIC_DIR.'backend/plugins/images/users/varun.jpg')}}" class="img img-responsive" alt="" style="width:50px;border-radius:50%;"><br> Ward No. 1</td>
                           <td style="padding-top:20px;"><strong style="background:#000;padding:5px 10px;"> 2 - 3</strong><h2 style="margin-top:5px;" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-chevron-circle-down arrow" id="model"></i></h2></td>
                           <td> <img src="{{asset(STATIC_DIR.'backend/plugins/images/users/varun.jpg')}}" class="img img-responsive" alt="" style="width:50px;border-radius:50%;"><br> Ward No. 2</td>
                       </tr>

                   </tbody>
               </div>
                    
    
        </table>
      </div>
      </div>
       <footer class="text-center" style="color:#ccc;">&copy Designed and Developed By Binod Thakur</footer>
      </div>

        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="{{ asset(STATIC_DIR.'js/app.js') }}" defer></script>
  </body>
  <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table text-center table-bordered table-striped" >
                              <thead>
                                <tr><th colspan="2">Score Details</th></tr>
                                <tr>
                                  <th >Ward No. 1 </th>
                                  <th >Ward No. 2 </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <ul style="list-style:none;text-align:left">
                                     <li>1.Binod Thakur 54'</li>
                                     <li>1. Binod Thakur Bomjam Bahadur54'</li>
                                    </ul>
                                   </td>
                                  <td>
                                    <ul >
                                    <li>1. Binod Thakur Bomjam Bahadur54'</li>
                                  </ul>

                                </td>
                                  
                                </tr>
                              </tbody>
                           </table>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
  
</html>