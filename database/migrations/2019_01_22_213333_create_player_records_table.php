<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->integer('stage_id')->unsigned();
            $table->integer('player_id')->unsigned();
            $table->integer('score_time')->unsigned()->nullable();
            $table->integer('score_position_id')->unsigned()->nullable();
            $table->integer('card_id')->unsigned()->nullable();
            $table->integer('in')->unsigned()->nullable();
            $table->integer('out')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_records');
    }
}
