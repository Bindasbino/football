<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stage_id')->unsigned();
            $table->integer('team1_id')->unsigned();
            $table->integer('team2_id')->unsigned();
            $table->integer('goal_1')->unsigned()->nullable();
            $table->integer('goal_2')->unsigned()->nullable();
            /*$table->integer('team1_player_id')->unsigned()->nullable();
            $table->integer('team2_plyer_id')->unsigned()->nullable();
            $table->integer('coach1_id')->unsigned()->nullable();
            $table->integer('coach2_id')->unsigned()->nullable();*/
            $table->integer('team_official_id')->unsigned()->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            /*$table->integer('end_time')->unsigned();*//*----End Time in updated timestamps*/
            $table->integer('status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
